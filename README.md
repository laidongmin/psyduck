# psyduck

#### 介绍
duck~ duck~ 一双充满睿智的眼神，写出来的微服务框架

#### 软件版本

当前对应版本的参考：https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E

|  组件名称   | 版本  |
|  ----  | ----  |
| Nacos  | 2.0.3 |
| Spring Cloud Alibaba  | 2.2.7.RELEASE |
| Spring Cloud  | Spring Cloud Hoxton.SR12 |
| Spring Boot Alibaba  | 2.3.12.RELEASE |




#### tips
m1架构无法直接使用docker启动nacos（原因是没有兼容arm架构）:
注意nacos2.x版本中新增了（8848+1000 and 8848+1001）端口来进行grpc通讯。（官方说明：https://nacos.io/zh-cn/docs/2.0.0-compatibility.html）
```shell
docker pull zhusaidong/nacos-server-m1:2.0.3
docker run --name nacos-standalone -e MODE=standalone -e JVM_XMS=512m -e JVM_XMX=512m -e JVM_XMN=256m -p 8848:8848 -p 9848:9848 -d zhusaidong/nacos-server-m1:2.0.3
```
