package cloud.pokemon.psyduckauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PsyduckAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(PsyduckAuthApplication.class, args);
    }

}
