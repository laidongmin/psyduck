package com.pokemon.psyduckgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PsyduckGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PsyduckGatewayApplication.class, args);
    }

}
