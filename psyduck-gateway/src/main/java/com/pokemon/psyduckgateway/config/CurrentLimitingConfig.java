package com.pokemon.psyduckgateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * 限流配置
 */
@Configuration
public class CurrentLimitingConfig {
    @Bean
    public KeyResolver ipKeyResolver() {
        return (ServerWebExchange exchange) ->
                Mono.just(Objects.requireNonNull(exchange.getRequest().getRemoteAddress()).getHostName())
                ;
    }
}
