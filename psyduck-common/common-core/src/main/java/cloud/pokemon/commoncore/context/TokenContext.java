package cloud.pokemon.commoncore.context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenContext {
    private static ThreadLocal<Map<String, List<String>>> haedersLocal = new InheritableThreadLocal<>();

    public static Map<String, List<String>> getHaedersLocal() {
        return haedersLocal.get();
    }

    public static void setHaedersLocal(Map<String, List<String>> haedersLocal) {
        TokenContext.haedersLocal.set(haedersLocal);
    }
}
